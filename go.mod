module opendev.org/vexxhost/rally-exporter

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/gophercloud/utils v0.0.0-20191212191830-4533a07bd492
	github.com/jinzhu/gorm v1.9.11
	github.com/prometheus/client_golang v0.9.3-0.20190127221311-3c4408c8b829
	github.com/prometheus/common v0.2.0
	github.com/stretchr/testify v1.5.1
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
